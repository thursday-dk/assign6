//********************************************************************
//File:         LockableDriver.java       
//Author:       Kalvin Dewey
//Date:         11/6/17
//Course:       CPS100
//
//Problem Statement:
//Write a Java interface called Lockable that includes the follow-
//ing methods: setKey, lock, unlock, and locked. The setKey,
//lock, and unlock methods take an integer parameter that repre-
//sents the key. The setKey method establishes the key. The lock
//and unlock methods lock and unlock the object, but only if the
//key passed in is correct. The locked method returns a boolean
//that indicates whether or not the object is locked. A Lockable
//object represents an object whose regular methods are protected:
//if the object is locked, the methods cannot be invoked; if it is
//unlocked, they can be invoked. Write a version of the Coin class
//from Chapter 5 so that it is Lockable.
//
//Inputs: none
//Outputs: exercise of Coin.java with Lockable interface
// 
//********************************************************************
public class LockableDriver
{
  public static void main(String[] args)
  {
    Coin coin = new Coin();

    System.out.println("coin init: " + coin);
    System.out.println("coin locked: " + coin.locked());
    System.out.println();

    coin.setKey(1337);
    coin.lock(1337);
    System.out.println("coin locked: " + coin.locked());
    System.out.println("coin value: " + coin);
    coin.flip();
    System.out.println("coin doesn't change: " + coin);
    System.out.println();

    coin.unlock(1337);
    System.out.println("unlock");
    coin.flip();
    System.out.println("coin can change: " + coin);
    System.out.println();

    System.out.println("coin is heads: " + coin.isHeads());
    coin.lock(1337);
    System.out.println("locked");
    System.out.println("coin always says false: " + coin.isHeads());
    System.out.println();

    coin.unlock(0);
    System.out.println("coin stays locked: " + coin.locked());
  }
}
