//********************************************************************
//  Coin.java       Author: Lewis/Loftus
//
//  Represents a coin with two sides that can be flipped.
//********************************************************************

public class Coin implements Lockable
{
  private final int HEADS = 0;
  private final int TAILS = 1;

  private int face;

  private int key = 0;
  private boolean locked = false;

  public void setKey(int key)
  {
    this.key = key;
  }

  public void lock(int key)
  {
    if (this.key == key)
      locked = true;
  }

  public void unlock(int key)
  {
    if (this.key == key)
      locked = false;
  }

  public boolean locked()
  {
    return locked;
  }

  //-----------------------------------------------------------------
  //  Sets up the coin by flipping it initially.
  //-----------------------------------------------------------------
  public Coin()
  {
    flip();
  }

  //-----------------------------------------------------------------
  //  Flips the coin by randomly choosing a face value.
  //-----------------------------------------------------------------
  public void flip()
  {
    if (!locked)
      face = (int) (Math.random() * 2);
  }

  //-----------------------------------------------------------------
  //  Returns true if the current face of the coin is heads.
  //-----------------------------------------------------------------
  public boolean isHeads()
  {
    return (face == HEADS && !locked);
  }

  //-----------------------------------------------------------------
  //  Returns the current face of the coin as a string.
  //-----------------------------------------------------------------
  public String toString()
  {
    if (!locked)
    {
      String faceName;

      if (face == HEADS)
        faceName = "Heads";
      else
        faceName = "Tails";

      return faceName;
    }
    else
      return "";
  }
}
