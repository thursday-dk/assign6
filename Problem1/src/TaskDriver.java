//********************************************************************
//File:         TaskDriver.java       
//Author:       Kalvin Dewey
//Date:         11/6/17
//Course:       CPS100
//
//Problem Statement:
//Write a Java interface called Priority that includes two methods:
//setPriority and getPriority. The interface should define
//a way to establish numeric priority among a set of objects.
//Design and implement a class called Task that represents a task
//(such as on a to-do list) that implements the Priority interface.
//Create a driver class to exercise some Task objects. 
//
//Inputs: none
//Outputs:  results of exercise
// 
//********************************************************************
public class TaskDriver
{
  public static void main(String[] args)
  {
    Task task1 = new Task(5, "task1");
    Task task2 = new Task(3, "task2");
    Task task3 = new Task(1, "task3");

    System.out.println("Task1 Priority: " + task1.getPriority());
    System.out.println("Task1 Task: " + task1.getTask());

    System.out.println("Task2 Priority: " + task2.getPriority());
    System.out.println("Task2 Task: " + task2.getTask());

    System.out.println("Task3 Priority: " + task3.getPriority());
    System.out.println("Task3 Task: " + task3.getTask());
    System.out.println();

    task3.setPriority(9);
    System.out.println("Task3 Changed Priority: " + task3.getPriority());
    System.out.println();

    task2.setTask("changed task");
    System.out.println("Task2 Task: " + task2.getTask());
    System.out.println();

    //example of priority testing
    if (task3.getPriority() > task1.getPriority())
      System.out.println("task3 has greater priority");
    else
      System.out.println("task1 has greater priority");
  }
}
