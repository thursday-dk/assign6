
public class Task implements Priority
{
  private int priority = 0;
  private String task = "No Task";

  public Task(int priority, String task)
  {
    this.priority = priority;
    this.task = task;
  }

  public void setPriority(int priority)
  {
    this.priority = priority;
  }

  public int getPriority()
  {
    return priority;
  }

  public String getTask()
  {
    return task;
  }

  public void setTask(String task)
  {
    this.task = task;
  }


}
